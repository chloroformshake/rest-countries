<h1 align="center">Where in the World?</h1>

<div align="center">
  <h3>
    <a href="https://cfs-rest-countries.netlify.app/">
      Hosted Page
    </a>
    <span> | </span>
    <span> | </span>
    <a href="https://gitlab.com/mountblue/js/rest-countries">
      Challenge Statement
    </a>
  </h3>
</div>

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

### Desktop Previews

![desktop_view1](./design/preview-rc1.png)
![desktop_view2](./design/preview-rc2.png)
![desktop_view3](./design/preview-rc3.png)


### Mobile Preview

#### Samsung Galaxy S9

![mobile_view1](./design/preview-rc4(samsung).jpg)

#### iPhone X
![mobile_view2](./design/preview-rc5(iphone).jpg)

### Tablet Preview (iPad)

![tablet_view](./design/preview-rc6(tablets).jpg)

## Built With

- [React](https://reactjs.org/)
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [Chakra UI](https://chakra-ui.com/)

## Features

This Web App named "Where in the World?" is made using the REST Countries API endpoints.
1) The features bar includes the options of Searching the countries by name and filtering them by Region.
2) It also has toggle button to switch between the dark and bright theme.
3) On clicking the the name on country's card, all the information about that country is displayed on a seperate Routed page.
4) One can navigate from one country's content page to its border countries' content page on clicking the corresponding button and can also go back to previous page.
5) It is responsive in nature.

## Contact

- GitHub [@chloroformshake](https://github.com/chloroformshake)
- Gitlab [@chloroformshake](https://gitlab.com/chloroformshake)
