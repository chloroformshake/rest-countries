import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import { FaLongArrowAltLeft } from 'react-icons/fa'

import { Button } from '@chakra-ui/button';
import { Box, Flex } from '@chakra-ui/layout';
import { useColorModeValue, Image, Text } from '@chakra-ui/react'

const Content = ({ item, countryData }) => {

    const color = useColorModeValue('#696969', 'white')

    const countryNameFind = (code) => countryData.filter(row => row.alpha3Code === code)[0].name

    const GoBack = () => {
        let history = useHistory();
        return <Button boxShadow={"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"} m={['50px', '70px']} w={['150px', '200px']} fontSize={['12px', '20px']} onClick={() => history.goBack()}><FaLongArrowAltLeft />&nbsp;&nbsp;&nbsp;&nbsp;Back</Button>
    }

    return (
        <Flex flexDirection={'column'} alignItems={'flex-start'}>

            <GoBack />
            <Box p={[4, 4, 4, 20]} mt={["-20px", "-40px", "-50px", "-60px"]} display={{ xl: "flex" }} w={'100%'}>

                <Flex flexShrink={0} flex={'1'} justifyContent={'center'} mb={'100px'} ><Image w={'90%'} h={'auto'} boxShadow={"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"} src={item.flag} alt={item.name} /></Flex>
                <Box flex={'1'} mt={{ base: 4, md: 0 }} ml={[4, 6, 10]}>

                    <Text fontSize={['30px', '35px', '40px']} my={'30px'} fontWeight={'extrabold'}>{item.name}</Text>
                    <Flex mt={1} fontSize={['12px', '15px', '15px', '18px']}>
                        
                        <Flex flexDirection={'column'} flex={'1'} mb={'50px'}>
                            <Flex flexWrap={'wrap'} mb={'10px'}>Native Name:&nbsp;&nbsp;<Text color={color}>{item.nativeName}</Text></Flex>
                            <Flex mb={'10px'}>Population:&nbsp;&nbsp;<Text color={color}>{item.population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Text></Flex>
                            <Flex mb={'10px'}>Region:&nbsp;&nbsp;<Text color={color}>{item.region}</Text></Flex>
                            <Flex mb={'10px'}>Sub Region:&nbsp;&nbsp;<Text color={color}>{item.subregion}</Text></Flex>
                            <Flex mb={'10px'}>Capital:&nbsp;&nbsp;<Text color={color}>{item.capital}</Text></Flex>
                        </Flex>
                        
                        <Flex flexDirection={'column'} flex={'1'} ml={['0px', '0px', '0px', '50px']}>
                            <Flex mb={'10px'}>Top Level Domain:&nbsp;&nbsp;{item.topLevelDomain.map(ele => <Text key={`${ele}`} color={color}>{ele}</Text>)}</Flex>
                            <Flex flexWrap={'wrap'} mb={'10px'}>Currencies:&nbsp;&nbsp;{item.currencies.map(ele => <Text key={ele.code} color={color}>{ele.name}&nbsp;</Text>)}</Flex>
                            <Flex flexWrap={'wrap'} mb={'10px'}>Languages:&nbsp;&nbsp;{item.languages.map(ele => <Text key={ele.iso639_2} color={color}>{ele.name}&nbsp;</Text>)}</Flex>
                        </Flex>

                    </Flex>
                    <Box mt={2} fontSize={['15px', '15px', '20px']}>Border Countries:&nbsp;&nbsp;{item.borders.map(ele => <Link to={`/${ele}`} key={`${ele}`}><Button m={'5px'}>{countryNameFind(ele)}</Button></Link>)}</Box>
                </Box>
            </Box>
        </Flex>
    )
}

export default Content
