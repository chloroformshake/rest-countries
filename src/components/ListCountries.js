import React from 'react'
import { Link } from 'react-router-dom'
import { Box, Flex, useColorModeValue } from '@chakra-ui/react'

const ListCountries = ({ countryData, searchTerm, filteredRegion }) => {

    const bg = useColorModeValue('#F8F8F8', '#332940')
    const color = useColorModeValue('#696969', 'white')

    return (
        <Flex justifyContent="center" flexWrap="wrap">

            {countryData
                .filter(item => {
                    if (filteredRegion === "all") { return true }
                    else { return filteredRegion === item.region }
                })
                .filter(item => {
                    if (searchTerm === "") { return true }
                    else if (item.name.toLowerCase().includes(searchTerm.toLowerCase())) { return true }
                    else { return false }
                })
                .map(item => (
                    <Box boxShadow={"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"} fontSize={'16px'} key={item.alpha3Code} bg={bg} width="300px" height="auto" m={50} borderRadius={'15px'}>
                        <img style={{ maxWidth: '90%', height: 'auto', margin: '15px' }} src={item.flag} alt={item.name} />
                        <Box m={10}>
                            <Link to={`/${item.alpha3Code}`}>
                                <Box mb={8} fontSize={'25px'} fontWeight={'bold'}>{item.name}</Box>
                            </Link>
                            <Flex>Population:&nbsp;<Box color={color}>{item.population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</Box></Flex>
                            <Flex>Region:&nbsp;<Box color={color}>{item.region}</Box></Flex>
                            <Flex>Capital:&nbsp;<Box color={color}>{item.capital}</Box></Flex>
                        </Box>
                    </Box>
                ))}
        </Flex>
    )
}

export default ListCountries
