import { IoMdSearch } from 'react-icons/io';
import styled from 'styled-components';

const SearchField = ({ searchTerm, setSearchTerm }) => {

    return (
        <>
            <Search>
                <IoMdSearch style={{ marginLeft: "1rem", position: "absolute" }} color="#623CEA" size="1.5em" />
                <SearchBar id="search-bar" type="text" placeholder="Search for a country..." value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} />
            </Search>
        </>
    )
}

const Search = styled.div`
  padding: .5rem;
  position: relative;
  display: flex;  
  align-items: center;
  width: 500px;
  height: 60px;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  margin-bottom: 30px;
  @media (max-width: 875px) {
      width: 90vw;
      height: 50px;
      right: 30px;
  }
`

const SearchBar = styled.input`
  padding: 1rem 1rem 1rem 3rem;
  width: 500px;
  height: 45px;
  background-color: #F8F8F8;
  @media (max-width: 875px) {
      width: 90vw;
      height: 40px;
  }
`

export default SearchField;