import React from 'react'
import { FaMoon, FaSun } from 'react-icons/fa'
import { Link } from 'react-router-dom'
import { Box, Button, Flex, useColorModeValue } from '@chakra-ui/react'

const Header = ({ colorMode, toggleColorMode }) => {

    return (
        <Flex w="100%" justifyContent={'space-between'} p={'30px'} borderBottom={'0.5px solid #C0C0C0'} bg={useColorModeValue('#F8F8F8', '#332940')} boxShadow={"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"}>
            <Link to="/"><Box ml={['0px', '5px', '20px']} fontSize={['20px', '30px', '30px']} fontWeight={"extrabold"}>Where in the World?</Box></Link>
            <Button fontSize={['15px', '18px', '18px']} mr={[0, 5, 20]} bg={'transparent'} onClick={toggleColorMode}>
                {colorMode === 'light' ? <><FaMoon />&nbsp;&nbsp;Dark Mode</> : <><FaSun />&nbsp;&nbsp;Bright Mode</>}
            </Button>
        </Flex>
    )
}

export default Header
