import React from "react";
import { Select } from '@chakra-ui/react'

export default function FilterBar({ countryData, filterRegion, setFilterRegion }) {

    const regionArrayExtract = (countryData) => {

        let array = []
        for (let index = 0; index < countryData.length; index++) { array.push(countryData[index].region) }
        array = array.filter(e => e !== "")

        return new Set(array)
    }

    const regionArray = [...regionArrayExtract(countryData)]

    return (
        <>
            <Select value={filterRegion} bg={"#f8f8f8"} boxShadow={"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"} w={'200px'} h={['40px', '60px']} color={"black"} size='lg' onChange={(e) => setFilterRegion(e.target.value)}>
                <option value="all">Filter by Region</option>)
                {regionArray.map(region => <option key={`${region}`} value={`${region}`}>{`${region}`}</option>)}
            </Select>
        </>
    );
}