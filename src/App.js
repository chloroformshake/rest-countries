import { useEffect, useState } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Box, useColorMode, useColorModeValue } from '@chakra-ui/react'

import './App.css'

import Header from './components/Header'
import FilterBar from './components/FilterBar'
import SearchField from './components/SearchField'
import ListCountries from './components/ListCountries'
import Content from './components/Content'

function App() {

  const { colorMode, toggleColorMode } = useColorMode()
  const [countryData, setCountryData] = useState([])
  const [searchTerm, setSearchTerm] = useState("")
  const [filterRegion, setFilterRegion] = useState("all")

  useEffect(() => {

    const getCountryData = () => {
      fetch("https://restcountries.com/v2/all")
        .then(response => response.json())
        .then(myJson => setCountryData(myJson))
    }
    getCountryData()

  }, [])

  return (
    <Router>

      <Header toggleColorMode={toggleColorMode} colorMode={colorMode} />

      <Box bg={useColorModeValue("transparent", "1f1a24")}>

        <Route path="/" exact render={() =>

        (<>
          <div id='feature-bar'>
            <SearchField searchTerm={searchTerm} setSearchTerm={setSearchTerm} />
            <FilterBar countryData={countryData} filterRegion={filterRegion} setFilterRegion={setFilterRegion} />
          </div>
          <ListCountries countryData={countryData} searchTerm={searchTerm} filteredRegion={filterRegion} />

        </>)} />
      </Box>

      {countryData.map(item => (

        <div key={item.alpha3Code}>
          <Route path={`/${item.alpha3Code}`} exact render={() => <Content item={item} countryData={countryData} />} />
        </div>

      ))}

    </Router>
  )
}

export default App;
